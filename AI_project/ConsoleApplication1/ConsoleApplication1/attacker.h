// attacker.h

#pragma once
#include "AI.h"
#include "Sprite.h"
#include "node.h"

#include <random>

#ifndef ATTACKER_H_
#define ATTACKER_H_

class Defender;

class Attacker : public AI /*, public Sprite */
{
public:
	Attacker();
	~Attacker();

	void update(float p_fdeltatime);
	void initialize(Node* p_nPlacement, const sf::RenderWindow* p_xWindow);
	void cleanup();
	//void lose();
	//void win();

	int get_hp();
	void give_dmg(int p_idmg);
	int get_dmg();

	Node* get_position() { return m_xCurrNode; }
	void set_position(Node* p_Node);

	AISTATE get_My_State();
	void set_My_State(AISTATE p_eState);

	//void pathfind_Defatt();

	bool IsType(std::string p_sType);
	void set_AttackTarget(AI* p_xTarget);
	AI* get_CurrentAttackTarget(); // you need to send in a pointer for this one.. had to do a special variant
	void clear_Target();

	std::vector<Node*> getNodePath();
	void setNodePath(std::vector<Node*> pv_inNodePath);
	void clearNodePath();
	void addToNodePath(Node* pn_inNode);
	void removeFromPath(int pi_placeNum);

private:
	
	// The target for pathfinding
	Defender* mx_Target;

	// The destination for each tile
	Node* mn_Destination;
	std::vector<Node*> mv_nodePath;
	// The current image each sprite is using
	Sprite ms_Sprite;
	Sprite m_sAttack;


	// Name generator
	int short mi_name_generator;
	std::string ms_attacker[3];
	// Properties
	int m_ihp;
	int m_idmg;

	AISTATE m_eMyState;
	Node* m_xCurrNode;
	std::string m_sType;

	const sf::RenderWindow* m_xWindow;

};


#endif // ATTACKER_H_