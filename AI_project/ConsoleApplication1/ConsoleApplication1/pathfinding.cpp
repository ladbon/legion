//pathfinding.cpp
#include "Sprite.h"
#include "pathfinding.h"
#include <iostream>

pathFinder::pathFinder()
{
	
}

pathFinder::~pathFinder()
{

}

std::vector<Node*> pathFinder::makePath(Node* pn_startNode, std::vector<Node*> pv_targetNodes)
{	
	m_xClosedNodes.clear();
	m_xOpenNodes.clear();
	m_xSearchPattern.clear();

	mn_curNode = nullptr;
	std::vector<Node*> vn_returnNodes;
	//Shamelessly ripped from Mike's lecture

	//initialize the open list(empty)
	std::vector<Node*> mv_openList;
	//initialize the closed list(empty)
	std::vector<Node*> mv_closedList;
	//put the starting node on the open list(you can leave its f at zero)
	mv_openList.push_back(pn_startNode);

	//Declare goal node //H
	Node* np_goalNode = GetGoalNode(pn_startNode, pv_targetNodes);

	//goal node nullptr = evrything is blocked
	if (np_goalNode == nullptr)
	{
		//g� bara �t h�ger
		if (pn_startNode->getNeiRgt() != nullptr && pn_startNode->getNeiRgt()->getOccupied() != true)
		{
			vn_returnNodes.push_back(pn_startNode->getNeiRgt());
		}
		return vn_returnNodes;

	}	

	//reset F values

	for (unsigned int i = 0; i < pv_targetNodes.size(); i++)
	{
		pv_targetNodes[i]->setF(0);
	}

	mn_curNode = nullptr; // moved this up and outside
	//while the open list is not empty

	unsigned int ui_limiter = 0;

	while (!mv_openList.empty() && ui_limiter < 50)
	{
		int index = 0;
		//find the node with the least total cost(f) on the open list
		int totalcost = 0;
		for (unsigned int i = mv_openList.size() -1; i < mv_openList.size(); i--)
		{
			if (i == mv_openList.size() - 1 || mv_openList[i]->getF() < totalcost) // changed this, it used to be current node. And it erased more stuff from the node list // Inge
			{
				//take this node off the open list and call it �curNode"
				mn_curNode = mv_openList[i];
				totalcost = mv_openList[i]->getF();
				index = i;
			}
		}
		mv_openList.erase(mv_openList.begin() + index); // that's why I moved this out. Openlist is bigger than it should, but at least it finds the nodes it should now. // Inge

		//find curNode's 8(Only 4 in our case. //Herman.) neighbors and set their parent to be to curNode
		std::vector<Node*> vn_Neighbours;

		if (mn_curNode->getNeiUp() != nullptr || !mn_curNode->getOccupied()) // added these, personal preference. Didn't seem to work for some reason though // Inge
		{
			vn_Neighbours.push_back(mn_curNode->getNeiUp());
		}

		if (mn_curNode->getNeiDwn() != nullptr || !mn_curNode->getOccupied())
		{
			vn_Neighbours.push_back(mn_curNode->getNeiDwn());
		}
		if (mn_curNode->getNeiLft() != nullptr || !mn_curNode->getOccupied())
		{
			vn_Neighbours.push_back(mn_curNode->getNeiLft());
		}
		if (mn_curNode->getNeiRgt() != nullptr || !mn_curNode->getOccupied())
		{
			vn_Neighbours.push_back(mn_curNode->getNeiRgt());
		}

		for (unsigned int z = 0; z < vn_Neighbours.size(); z++)
		{
			if (vn_Neighbours[z] != nullptr)
			{
				//Making sure the node isn't allready in closedlist
				if (mv_closedList.size() > 0)
				{
					bool inList = false;
					for (unsigned int y = 0; y < mv_closedList.size(); y++)
					{
						if (vn_Neighbours[z]->getNum() == mv_closedList[y]->getNum())
						{
							inList = true;
						}
					}
					if (inList == false)
					{
						vn_Neighbours[z]->setParent(mn_curNode);
					}
				}
				else
				{
					vn_Neighbours[z]->setParent(mn_curNode);
				}
			}
		}

		//std::cout << mv_closedList.size() << std::endl;

		//for each neighbor
		for (unsigned int a = 0; a < vn_Neighbours.size(); a++)
		{
			if (vn_Neighbours[a] != nullptr)
			{
				
				// if neighbor is the goal, stop the search     // or finish searching first...
				if (vn_Neighbours[a]->getNum() == np_goalNode->getNum())
				{
					mv_closedList.push_back(mn_curNode);
					mv_closedList.push_back(vn_Neighbours[a]);
					mn_curNode = nullptr;
					mv_openList.clear();
					break;
				}

				//neighbor.g = curNode.g + cost to go from curNode to neighbor
				vn_Neighbours[a]->setG(mn_curNode->getG() + vn_Neighbours[a]->getTerrain()); // changed this.
				//vn_Neighbours[a]->setG(vn_Neighbours[a]->getG() + vn_Neighbours[a]->getTerrain()); 


				//neighbor.h = estimated distance from neighbor to goal'
				vn_Neighbours[a]->setH(abs((int)vn_Neighbours[a]->getPos().x - (int)np_goalNode->getPos().x) +
					abs((int)vn_Neighbours[a]->getPos().y - (int)np_goalNode->getPos().y));

				//neighbor.f = neighbor.g + neighbor.h
				vn_Neighbours[a]->addGH();

				bool addIt = true;

				//if the node is occupied (we checked if it's the goal erlier)
				if (vn_Neighbours[a]->getOccupied())
				{
					addIt = false;
				}

				if (addIt)
				{
					//if a node in the OPEN list has a lower total cost(f) than neighbor,
					for (unsigned int b = 0; b < mv_openList.size(); b++)
					{
						if (mv_openList[b]->getF() < vn_Neighbours[a]->getF())
						{
							//skip this neighbor    // we have a better candidate already
							addIt = false;
						}
					}

					//if a node in the CLOSED list has a lower total cost(f) than neighbor,
					for (unsigned int c = 0; c < mv_closedList.size(); c++)
					{
						// removed the skip neighbor thing. This was the culprit really // Inge
						if (/*mv_closedList[c]->getF() < vn_Neighbours[a]->getF() ||*/ mv_closedList[c]->getNum() == vn_Neighbours[a]->getNum())
						{
							//skip this neighbor    // we have a better path already
							addIt = false;
						}
					}
				}

				//else
				if (addIt)
				{
					//add the node to the open list
					mv_openList.push_back(vn_Neighbours[a]);
					m_xOpenNodes.push_back(vn_Neighbours[a]); // this is to draw the stuff out. 
#ifdef _DEBUG
					std::cout << "Openlist size: " << mv_openList.size() << std::endl;
#endif
				}

			} // end  for loop
		}

		//put curNode on the closed list
		mv_closedList.push_back(mn_curNode);
		ui_limiter++;
#ifdef _DEBUG
		std::cout << "Closedlist size: " << mv_closedList.size() << std::endl;
#endif
		m_xClosedNodes = mv_closedList;
	}
	// end while
	
	for (int i = 0; i < m_xOpenNodes.size(); i++)
	{
		Sprite openSprite = m_xOpen;
		openSprite.set_position(m_xOpenNodes[i]->getPos());
		m_xSearchPattern.push_back(openSprite);
	}

	for (int i = 0; i < m_xClosedNodes.size(); i++)
	{
		if (m_xClosedNodes[i] == nullptr)
		{
			continue;
		}
		Sprite closedSprite = m_xClosed;
		closedSprite.set_position(m_xClosedNodes[i]->getPos());
		m_xSearchPattern.push_back(closedSprite);
	}

	
	//std::cout << "Goalnode number: " << np_goalNode->getNum() << "  Parent number: " << np_goalNode->getParent()->getNum() << std::endl;

	for (unsigned int i = 0; i < mv_closedList.size(); i++)
	{

#ifdef _DEBUG
		if (mv_closedList[i]->getParent() != nullptr)
			std::cout << "Closedlist " << i << " Number: " << mv_closedList[i]->getNum() << " Parent's Number: " << mv_closedList[i]->getParent()->getNum() << std::endl;
#endif

		//if we have found the goal
		if (mv_closedList[i]->getNum() == np_goalNode->getNum())
		{
			//retrace path from goal back to start via each node�s parent
	
			if (mv_closedList[0]->getParent() != nullptr)
			{
				mv_closedList[0]->setParent(nullptr);
			}

			Node* myNode = mv_closedList[i];

			while (myNode->getParent() != nullptr)
			{
				vn_returnNodes.push_back(myNode);
				myNode = myNode->getParent();
			}

			return vn_returnNodes;
		}
	}

	//easy exit for debug reasons
	//return vn_returnNodes;
	


	//What to do when the goal isn't found//H

	//(No longer needed because of the earlier check, commented out for that reason)
	//If all goals are blocked
	//if (pv_targetNodes.size() == 1)
	//{
	//	Node* np_closest = nullptr;
	//	
	//	//Find the node in the list closest to the goal node
	//	for (unsigned int i = 0; i < mv_closedList.size(); i++)
	//	{
	//		if (np_closest == nullptr || mv_closedList[i]->getH() < np_closest->getH())
	//		{
	//			np_closest = mv_closedList[i];
	//		}
	//	}
	//	vn_returnNodes.push_back(np_closest);
	//	//return the node closest to the closest goal
	//	return makePath(pn_startNode, vn_returnNodes);
	//}

	//remove the current goal from the list, it's blocked so we want to find the next one. //H
	for (unsigned int i = 0; i < pv_targetNodes.size(); i++)
	{
		if (pv_targetNodes[i]->getNum() == np_goalNode->getNum())
		{
			pv_targetNodes.erase(pv_targetNodes.begin() + i);
		}
	}

	if (pv_targetNodes.size() == 0)
	{
		return pv_targetNodes;
	}

	return (makePath(pn_startNode, pv_targetNodes));

	//Junk
	//if (makePath(pn_startNode, pv_targetNodes).size() == 0)
	//{
	//	for (unsigned int i = 0; i < mv_closedList.size(); i++)
	//	{
	//		//if we have found the goal
	//		if (mv_closedList[i]->getNum() == np_goalNode->getNum())
	//		{
	//			//retrace path from goal back to start via each node�s parent

	//			Node* myNode = np_goalNode;

	//			while (myNode->getParent() != nullptr)
	//			{
	//				vn_returnNodes.push_back(myNode);
	//				myNode = myNode->getParent();
	//			}
	//			return vn_returnNodes;
	//		}
	//	}
	//}

	//return makePath(pn_startNode, pv_targetNodes);

}
//Kom ih�g att nolst�lla variabler i varje Node efterr�t. //Herman


void pathFinder::Initialize(const sf::RenderWindow* p_xWindow)
{
	m_xWindow = p_xWindow;
	m_xOpen.set_image("../images/OpenList.png", sf::IntRect(-50, -50, 32, 32));
	m_xClosed.set_image("../images/ClosedList.png", sf::IntRect(-50, -50, 32, 32));

}

void pathFinder::Update(float delta)
{
	

}

void pathFinder::Draw()
{
	for (unsigned int i = 0; i < m_xSearchPattern.size(); i++)
	{
		m_xSearchPattern[i].draw_image(m_xWindow);
	}

}


Node* pathFinder::GetGoalNode(Node* p_xCurrentPosition, std::vector<Node*> p_xTargetNodes)
{
	Node* np_goalNode = nullptr;
	for (unsigned int i = 0; i < p_xTargetNodes.size(); i++)
	{
		//Just to find the closest one //H
		p_xTargetNodes[i]->setF(abs((int)p_xCurrentPosition->getPos().x - (int)p_xTargetNodes[i]->getPos().x)
			+ abs((int)p_xCurrentPosition->getPos().y - (int)p_xTargetNodes[i]->getPos().y));

		// adding a "fix" - bad one though // Inge
		if (np_goalNode == nullptr || p_xTargetNodes[i]->getF() < np_goalNode->getF())
		{
			np_goalNode = p_xTargetNodes[i];
		}
	}
	
	bool bLeft = true, bRight = true, bUp = true, bDown = true;
	if (np_goalNode == nullptr)
	{
		return nullptr;
	}
	if (np_goalNode->getNeiLft() != nullptr)
	{
		if (np_goalNode->getNeiLft()->getOccupied())
		{
			bLeft = false;
		}
	}
	else {
		bLeft = false;
	}

	if (np_goalNode->getNeiUp() != nullptr)
	{
		if (np_goalNode->getNeiUp()->getOccupied())
		{
			bUp = false;
		}
	}
	else {
		bUp = false;
	}

	if (np_goalNode->getNeiDwn() != nullptr)
	{
		if (np_goalNode->getNeiDwn()->getOccupied())
		{
			bDown = false;
		}
	}
	else {
		bDown = false;
	}

	if (np_goalNode->getNeiRgt() != nullptr)
	{
		if (np_goalNode->getNeiRgt()->getOccupied())
		{
			bRight = false;
		}
	}
	else {
		bRight = false;
	}
	//if (np_goalNode->getPos().x > p_xCurrentPosition->getPos().x)
	//{
		if (bLeft)
		{
			return np_goalNode;
		}
		//}
		//if (np_goalNode->getPos().x < p_xCurrentPosition->getPos().x)
		//{
		if (bRight)
		{
			return np_goalNode;
		}
		//	}
		//	if (np_goalNode->getPos().y < p_xCurrentPosition->getPos().y)
		//{
		if (bUp)
		{
			return np_goalNode;
		}
		//}
		//if (np_goalNode->getPos().y > p_xCurrentPosition->getPos().y)
		//{
		if (bDown)
		{
			return np_goalNode;
		}
		//}

	if (p_xTargetNodes.size() > 0)
	{
		std::vector<Node*> tempList = p_xTargetNodes;
		for (int i = 0; i < tempList.size(); i++)
		{
			if (tempList[i] == np_goalNode)
			{
				tempList.erase(tempList.begin() + i);
				break;
			}
		}
		np_goalNode = GetGoalNode(p_xCurrentPosition, tempList);
	}

	return np_goalNode;
}