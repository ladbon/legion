//Sprite.h
// Ladbon
#pragma once

#include "SFML\Graphics.hpp"
#include <string>


class Sprite
{
public:
	Sprite();
	~Sprite();

	void set_image(std::string p_sFilename, sf::IntRect p_iPosition_size);
	void set_position(sf::Vector2f p_vPosition);
	void draw_image(const sf::RenderWindow* p_rWindow);
	sf::Sprite& get_sprite();

private:
	sf::Image m_iImage;
	sf::Texture m_tTexture;
	sf::Sprite m_sSprite;
};

