// defender.h

#pragma once

#include "AI.h"
#include "Sprite.h"
#include "node.h"

#include <random>


#ifndef DEFENDER_H_
#define DEFENDER_H_

class Attacker;

class Defender : public AI /*, public Sprite */
{
public:
	Defender();
	~Defender();

	void update(float p_fdeltatime);
	void initialize(Node* p_nPlacement, const sf::RenderWindow* p_xWindow);
	void cleanup();
	//void win();
	//void lose();
	bool IsType(std::string p_sType);

	int get_hp();
	void give_dmg(int p_idmg);
	int get_dmg();

	AISTATE get_My_State();
	void set_My_State(AISTATE p_eState);
	//void pathfind_Defatt();

	Node* get_position() { return m_xCurrNode; }
	void set_position(Node* p_xNode);

	void set_AttackTarget(AI* p_xTarget);
	AI* get_CurrentAttackTarget();
	void clear_Target();

private:
	Attacker* mx_Target;
	Node* mn_Destination;
	Sprite ms_Sprite;
	Sprite m_sAttack;
	int short mi_name_generator;

	std::string ms_defender[3];

	// Properties
	int m_ihp;
	int m_idmg;

	AISTATE m_eMyState;
	//pathFinder m_xPathFinder;
	Node* m_xCurrNode;
	std::string m_sType;
	const sf::RenderWindow* m_xWindow;
};


#endif // !DEFENDER_H_