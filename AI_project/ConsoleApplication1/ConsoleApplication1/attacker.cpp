//Attacker.cpp

#include <string>
#include <random>
#include "Sprite.h"
#include "attacker.h"
#include "defender.h"

Attacker::Attacker()
{
	//	ms_Sprite = new Sprite();
	//mx_Target = new Attacker();
	mn_Destination = nullptr;
	// name generator
	static std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, 2);
	mi_name_generator = distribution(generator);

	ms_attacker[0] = "../images/attacker1.png";
	ms_attacker[1] = "../images/attacker2.png";
	ms_attacker[2] = "../images/attacker3.png";
	m_sType = "Attacker";

	mv_nodePath = std::vector<Node*>{};

	m_idmg = 10;
	m_ihp = 100;
	m_eMyState = AISTATE::MOVE;
}

Attacker::~Attacker()
{

}

void Attacker::update(float p_fdeltatime)
{

	/*
	if(attackers position in inside the base tiles)
	{
	lose();
	}
	*/
	if (m_eMyState == AISTATE::ATTACK)
	{
		m_sAttack.draw_image(m_xWindow);
	}
	else
	{
		ms_Sprite.draw_image(m_xWindow);
	}
}

void Attacker::initialize(Node* p_nPlacement, const sf::RenderWindow* p_xWindow)
{
	m_xCurrNode = p_nPlacement;
	ms_Sprite.set_image(ms_attacker[mi_name_generator], sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	
	if (mi_name_generator == 0)
	{
		m_sAttack.set_image("../images/attacker1_attack.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	}
	else if (mi_name_generator == 1)
	{
		m_sAttack.set_image("../images/attacker2_attack.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	}
	else if (mi_name_generator == 2)
	{
		m_sAttack.set_image("../images/attacker3_attack.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	}

	m_xWindow = p_xWindow;

}

void Attacker::cleanup()
{
	m_xCurrNode->setOccupied(false);
}

//void Attacker::lose()
//{
//	/*
//	   sprite->set_image(../images/lose.png, sf::IntRect(0, 0, 960, 480));
//	*/
//}

int Attacker::get_hp()
{
	return m_ihp;
}

void Attacker::give_dmg(int p_idmg)
{
	m_ihp -= p_idmg;
	if (m_ihp <= 0) // might as well set this here
	{
		m_eMyState = AISTATE::DEAD;
		if (mx_Target != nullptr)
		{
			//mx_Target
		}
	}

}

int Attacker::get_dmg()
{
	return m_idmg;
}

bool Attacker::IsType(std::string p_sType)
{
#ifdef _DEBUG
	static int once = 0;
	if (once == 0)
		printf("Attacker type - %s\n\r", m_sType.c_str());
	once++;
#endif // _DEBUG

	return p_sType.compare(m_sType) == 0;
}

//void Attacker::pathfind_Defatt()
//{
//
//}

AISTATE Attacker::get_My_State()
{
	return m_eMyState;
}

void Attacker::set_My_State(AISTATE p_eState)
{
	m_eMyState = p_eState;
}

void Attacker::set_position(Node* p_xNode)
{
	if (!p_xNode->getOccupied())
	{
		m_xCurrNode->setOccupied(false);
		m_xCurrNode = p_xNode;
		m_xCurrNode->setOccupied(true);
		ms_Sprite.set_position(m_xCurrNode->getPos());
		m_sAttack.set_position(m_xCurrNode->getPos());
	}
}

void Attacker::set_AttackTarget(AI* p_xTarget)
{
	mx_Target = (Defender*)p_xTarget;
}


AI* Attacker::get_CurrentAttackTarget()
{
	return (AI*)mx_Target;
}

void Attacker::clear_Target()
{
	if (mx_Target != nullptr)
	{
		mx_Target->set_AttackTarget(NULL);
	}
	mx_Target = nullptr;
}

std::vector<Node*> Attacker::getNodePath()
{
	return mv_nodePath;
};
void Attacker::setNodePath(std::vector<Node*> pv_inNodePath)
{
	mv_nodePath = pv_inNodePath;
};

void Attacker::clearNodePath()
{
	mv_nodePath.clear();
}

void Attacker::addToNodePath(Node* pn_inNode)
{
	mv_nodePath.push_back(pn_inNode);
};

void Attacker::removeFromPath(int pi_placeNum)
{
	mv_nodePath.erase(mv_nodePath.begin() + pi_placeNum);
};