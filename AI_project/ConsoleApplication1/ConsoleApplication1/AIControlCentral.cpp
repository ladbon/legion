#include "AI.h"
#include "attacker.h"
#include "defender.h"
#include "pathfinding.h"
#include <vector>
#include "AIControlCentral.h"
#include <windows.h>

AIControlCentral::AIControlCentral()
{

}


AIControlCentral::~AIControlCentral()
{
	cleanup();
}

void AIControlCentral::initialize(const sf::RenderWindow* p_xWindow, std::vector<Node*> p_avNodes)
{
	m_fAgentActionInterval = 1.0f; // how often the attackers attack
	m_fAgentActionTimer = m_fAgentActionInterval; // the actual counter. So you only need to set the interval
	m_iMaxDefenders = 10; // change this for the amount of defenders you want to have in the game. can be changed from the outside.
	m_iMaxAttackers = 19;
	m_iMaxWalls = 4;
	m_iCurrentDefenders = 0;
	m_iCurrentAttackers = 0;
	m_iCurrentWalls = 0;
	m_xWindow = p_xWindow;
	m_avNodes = p_avNodes;
	m_bStartGame = false;
	m_bInnerWorking = false;
	m_bInGoal = false;
	m_xPathFinder.Initialize(p_xWindow);
}

void AIControlCentral::draw_InnerWorkings()
{
	if (!m_bInnerWorking)
		m_bInnerWorking = true;
	else
		m_bInnerWorking = false;
}

bool  AIControlCentral::check_Goal()
{
	return m_bInGoal;
}

void AIControlCentral::update(float p_fDelta)
{
	m_fAgentActionTimer -= p_fDelta;

	for (unsigned int i = 0; i < m_xAgents.size(); i++)
	{
		// loop through to figure out what they do.
		if (m_fAgentActionTimer <= 0.0f && m_bStartGame) // actions update every second.
		{
			agentsActions(m_xAgents[i]);
		}

	}

	// needs to come before the update, Agents update will put the images in the draw buffer, then once you take out the 
	// sprite and then try to display on screen the sprite will be deleted and display draw will crash.

	if (m_fAgentActionTimer <= 0.0f)
	{
		m_fAgentActionTimer = m_fAgentActionInterval;
		remove_DeadAgents();
	}

	for (unsigned int i = 0; i < m_xAgents.size(); i++)
	{
		m_xAgents[i]->update(p_fDelta);
		
		if (m_xAgents[i]->get_position()->getIsGoal() == true)
		{ 
			m_bInGoal = true;
		}
		if (m_bInnerWorking)
			m_xPathFinder.Draw();
	}
	for (unsigned int i = 0; i < m_xWalls.size(); i++)
	{
		m_xWalls[i]->update(p_fDelta);
	}


}


void AIControlCentral::cleanup()
{
	for (unsigned int i = 0; i < m_xAgents.size(); i++)
	{
		delete m_xAgents[i];
		m_xAgents[i] = nullptr;
	}
	m_xAgents.clear();

	for (unsigned int i = 0; i < m_xWalls.size(); i++)
	{
		delete m_xWalls[i];
		m_xWalls[i] = nullptr;
	}
	m_xWalls.clear();
}

void AIControlCentral::agentsActions(AI* p_xAgent)
{

	if (p_xAgent->IsType("Attacker"))
	{
		Attacker* xAttacker = (Attacker*)p_xAgent; // so we can get the specific methods for the attacker

		switch (p_xAgent->get_My_State())
		{
		case AISTATE::ATTACK:

			// determine target
			// attack target until this agent is dead or target is dead.
			attacker_Attack(xAttacker);
			break;

		case AISTATE::MOVE:
			// use pathfinder to find path. This is more or less why we have distinction between attacker and defender.

			attacker_Move(xAttacker);
			break;

		case AISTATE::BLOCKED:
			// check if target is available or if I can move. If not do nothing.
			attacker_Blocked(xAttacker);
			break;

		case AISTATE::DEAD:
			// I am dead. We can change sprite here or just delete the agent.
			// had to remove this part, because it removed while going through the list of agents and it got messy. Now we have more "control"
			//attacker_Dead(xAttacker);


			break;

		default:
			// suggest to set the default to move.
			p_xAgent->set_My_State(AISTATE::MOVE);
			break;
		}

	}
	else if (p_xAgent->IsType("Defender"))
	{
		Defender* xDefender = (Defender*)p_xAgent; // so we can get the specific methods for the defender
		switch (p_xAgent->get_My_State())
		{
		case AISTATE::ATTACK:

			// determine target
			// attack target until this agent is dead or target is dead.
			defender_Attack(xDefender);
			break;

		case AISTATE::MOVE:
			// use pathfinder to find path. This is more or less why we have distinction between attacker and defender.
			defender_Move(xDefender);
			break;

		case AISTATE::BLOCKED:
			// check if target is available or if I can move. If not do nothing -> go to idle?.
			defender_Blocked(xDefender);
			break;

		case AISTATE::DEAD:
			// I am dead. We can change sprite here or just delete the agent.
			//defender_Dead(xDefender);
			break;

		case AISTATE::IDLE:
			// check vicinity
			// if nobody, do nothing. If one attacker is in vicinity
			// move to attack.
			defender_Idle(xDefender);
			break;

		default:
			// suggest to set the default to move.
			p_xAgent->set_My_State(AISTATE::MOVE);
			break;
		}
	}



}

void AIControlCentral::attacker_Attack(Attacker* p_xAgent)
{
	Defender* xTarget = (Defender*)p_xAgent->get_CurrentAttackTarget();
	if (xTarget != nullptr)
	{
		xTarget->give_dmg(10);
	}
	else {
		p_xAgent->set_My_State(AISTATE::MOVE);
	}
}

void AIControlCentral::attacker_Move(Attacker* p_xAgent)
{
	//nodepath is empty
	if (p_xAgent->getNodePath().size() == 0 || p_xAgent->getNodePath()[0]->getOccupied())
	{
		//empty path
		p_xAgent->clearNodePath();
		//make the path 

		//get the list of opposing units (and then the base)

		std::vector<Node*> vn_inputNodes;

		for (unsigned int i = 0; i < m_xAgents.size(); i++)
		{
			if (m_xAgents[i]->IsType("Defender"))
			{
				vn_inputNodes.push_back(m_xAgents[i]->get_position());
			}
		}
		//Also needs to get the base.
		if (vn_inputNodes.size() == 0)
		{
			for (unsigned int i = 0; i < m_avNodes.size(); i++)
			{
				if (m_avNodes[i]->getIsGoal())
				{
					vn_inputNodes.push_back(m_avNodes[i]);
				}
			}
		}

		//Make a path (vector<Node*>)
		p_xAgent->setNodePath( m_xPathFinder.makePath(p_xAgent->get_position(), vn_inputNodes));
	}
	
	//nodepath is not blocked 

	//move to the last node in the path
	if (p_xAgent->getNodePath().size() != 0)
	{
		//move
		p_xAgent->set_position(p_xAgent->getNodePath()[p_xAgent->getNodePath().size() - 1]);

		//empty path and reset nodes' variables
		p_xAgent->clearNodePath();
		for (unsigned int i = 0; i < m_avNodes.size(); i++)
		{
			m_avNodes[i]->resetPath();
		}
	}
	else if (p_xAgent->get_position()->getNeiRgt() != nullptr)
	{
		//move right
		if (p_xAgent->get_position()->getNeiRgt()->getOccupied() != true)
		{
			//move
			p_xAgent->set_position(p_xAgent->get_position()->getNeiRgt());

			//empty path and reset nodes' variables
			p_xAgent->clearNodePath();
			for (unsigned int i = 0; i < m_avNodes.size(); i++)
			{
				m_avNodes[i]->resetPath();
			}
		}
	}
	//check if near an enemy
	detect_enemy(p_xAgent, p_xAgent->get_position());





	// Hermans (gamla) delar.
	//get the list of opposing units (and then the base)

	//std::vector<Node*> vn_inputNodes;

	//for (unsigned int i = 0; i < m_xAgents.size(); i++)
	//{
	//	if (m_xAgents[i]->IsType("Defender"))
	//	{
	//		vn_inputNodes.push_back(m_xAgents[i]->get_position());
	//	}
	//}
	////Also needs to get the base.
	//if (vn_inputNodes.size() == 0)
	//{
	//	for (unsigned int i = 0; i < m_avNodes.size(); i++)
	//	{
	//		if (m_avNodes[i]->getIsGoal())
	//		{
	//			vn_inputNodes.push_back(m_avNodes[i]);
	//		}
	//	}
	//}

	////Make a path (vector<Node*>)
	//std::vector<Node*> walkpath = m_xPathFinder.makePath(p_xAgent->get_position(), vn_inputNodes);

	////move to the last node in the path
	//if (walkpath.size() != 0 && walkpath[walkpath.size() - 1] != nullptr && walkpath[walkpath.size() - 1]->getOccupied() == false)
	//{
	//	//move
	//	p_xAgent->set_position(walkpath[walkpath.size() - 1]);

	//	//empty path and reset nodes' variables
	//	walkpath.clear();
	//	for (unsigned int i = 0; i < m_avNodes.size(); i++)
	//	{
	//		m_avNodes[i]->resetPath();
	//	}
	//}
	////check if near an enemy
	//detect_enemy(p_xAgent, p_xAgent->get_position());

	//Old "walk right" code
	/*if (p_xAgent->get_position()->getNeiRgt() != nullptr && !p_xAgent->get_position()->getNeiRgt()->getOccupied())
	{
	p_xAgent->set_position(p_xAgent->get_position()->getNeiRgt());
	}
	*/
}


void AIControlCentral::attacker_Blocked(Attacker* p_xAgent)
{

}

void AIControlCentral::attacker_Dead(Attacker* p_xAgent)
{
	for (auto it = m_xAgents.begin(); it != m_xAgents.end(); it++)
	{
		if ((*it) == p_xAgent)
		{
			m_xAgents.erase(it);
			break;
		}
	}
	p_xAgent->cleanup();
	p_xAgent->get_CurrentAttackTarget()->set_AttackTarget(nullptr);
	delete p_xAgent;
	p_xAgent = nullptr;
}

void AIControlCentral::defender_Attack(Defender* p_xAgent)
{
	Attacker* xTarget = (Attacker*)p_xAgent->get_CurrentAttackTarget();
	if (xTarget != nullptr)
	{
		xTarget->give_dmg(30);
	}
	else {
		p_xAgent->set_My_State(AISTATE::IDLE);
	}
}

void AIControlCentral::defender_Move(Defender* p_xAgent)
{
	//get the list of opposing units (and then the base)

	std::vector<Node*> vn_inputNodes;

	for (unsigned int i = 0; i < m_xAgents.size(); i++)
	{
		if (m_xAgents[i]->IsType("Attacker"))
		{
			vn_inputNodes.push_back(m_xAgents[i]->get_position());
		}
	}
	//Also needs to get the base.


	//Make a path (vector<Node*>)
	std::vector<Node*> walkpath = m_xPathFinder.makePath(p_xAgent->get_position(), vn_inputNodes);

	//move to the last node in the path
	if (walkpath.size() != 0 && walkpath[walkpath.size() - 1] != nullptr && walkpath[walkpath.size() - 1]->getOccupied() == false)
	{
		//move
		p_xAgent->set_position(walkpath[walkpath.size() - 1]);

		//empty path and reset nodes' variables
		walkpath.clear();
		for (unsigned int i = 0; i < m_avNodes.size(); i++)
		{
			m_avNodes[i]->resetPath();
		}
	}
	//check if near an enemy
	detect_enemy(p_xAgent, p_xAgent->get_position());
}

void AIControlCentral::defender_Idle(Defender* p_xAgent)
{
	detect_enemy(p_xAgent, p_xAgent->get_position());
}

void AIControlCentral::defender_Dead(Defender* p_xAgent)
{
	m_iCurrentDefenders--;

	for (auto it = m_xAgents.begin(); it != m_xAgents.end(); it++)
	{
		if ((*it) == p_xAgent)
		{
			m_xAgents.erase(it);
			break;
		}
	}
	p_xAgent->cleanup();
	p_xAgent->get_CurrentAttackTarget()->set_AttackTarget(nullptr);
	delete p_xAgent;
	p_xAgent = nullptr;

}

void AIControlCentral::defender_Blocked(Defender* p_xAgent)
{

}

void AIControlCentral::detect_enemy(AI* p_xAgent, Node* p_xCurrLocation)
{
	if (p_xCurrLocation->getNeiRgt() != nullptr && p_xCurrLocation->getNeiRgt()->getOccupied())
	{
		for (unsigned int i = 0; i < m_xAgents.size(); i++)
		{
			if (m_xAgents[i]->get_position() == p_xCurrLocation->getNeiRgt())
			{
				if ((p_xAgent->IsType("Attacker") && m_xAgents[i]->IsType("Defender")) ||
					(m_xAgents[i]->IsType("Attacker") && p_xAgent->IsType("Defender")))
				{
					p_xAgent->set_My_State(AISTATE::ATTACK);
					p_xAgent->set_AttackTarget(m_xAgents[i]);
					printf("Enemy detected\n\r");
					if (p_xAgent->IsType("Defender"))
					{
						printf("Defender\n\r");
					}
					return;
				}
				else {
					if (p_xAgent->IsType("Attacker"))
					{
						p_xAgent->set_My_State(AISTATE::MOVE);
					}
					else if (p_xAgent->IsType("Defender"))
					{
						p_xAgent->set_My_State(AISTATE::IDLE);
					}
				}
			}
		}

	}
	if (p_xCurrLocation->getNeiLft() != nullptr && p_xCurrLocation->getNeiLft()->getOccupied())
	{
		for (unsigned int i = 0; i < m_xAgents.size(); i++)
		{
			if (m_xAgents[i]->get_position() == p_xCurrLocation->getNeiLft())
			{
				if ((p_xAgent->IsType("Attacker") && m_xAgents[i]->IsType("Defender")) ||
					(m_xAgents[i]->IsType("Attacker") && p_xAgent->IsType("Defender")))
				{
					p_xAgent->set_My_State(AISTATE::ATTACK);
					p_xAgent->set_AttackTarget(m_xAgents[i]);
					printf("Enemy detected\n\r");
					if (p_xAgent->IsType("Defender"))
					{
						printf("Defender\n\r");
					}
					return;
				}
				else {
					if (p_xAgent->IsType("Attacker"))
					{
						p_xAgent->set_My_State(AISTATE::MOVE);
					}
					else if (p_xAgent->IsType("Defender"))
					{
						p_xAgent->set_My_State(AISTATE::IDLE);
					}
				}
			}
		}
	}

	if (p_xCurrLocation->getNeiDwn() != nullptr && p_xCurrLocation->getNeiDwn()->getOccupied())
	{
		for (unsigned int i = 0; i < m_xAgents.size(); i++)
		{
			if (m_xAgents[i]->get_position() == p_xCurrLocation->getNeiDwn())
			{
				if ((p_xAgent->IsType("Attacker") && m_xAgents[i]->IsType("Defender")) ||
					(m_xAgents[i]->IsType("Attacker") && p_xAgent->IsType("Defender")))
				{
					p_xAgent->set_My_State(AISTATE::ATTACK);
					p_xAgent->set_AttackTarget(m_xAgents[i]);
					printf("Enemy detected\n\r");
					if (p_xAgent->IsType("Defender"))
					{
						printf("Defender\n\r");
					}
					return;
				}
				else {
					if (p_xAgent->IsType("Attacker"))
					{
						p_xAgent->set_My_State(AISTATE::MOVE);
					}
					else if (p_xAgent->IsType("Defender"))
					{
						p_xAgent->set_My_State(AISTATE::IDLE);
					}
				}
			}
		}

	}

	if (p_xCurrLocation->getNeiUp() != nullptr && p_xCurrLocation->getNeiUp()->getOccupied())
	{
		for (unsigned int i = 0; i < m_xAgents.size(); i++)
		{
			if (m_xAgents[i]->get_position() == p_xCurrLocation->getNeiUp())
			{
				if ((p_xAgent->IsType("Attacker") && m_xAgents[i]->IsType("Defender")) ||
					(m_xAgents[i]->IsType("Attacker") && p_xAgent->IsType("Defender")))
				{
					p_xAgent->set_My_State(AISTATE::ATTACK);
					p_xAgent->set_AttackTarget(m_xAgents[i]);
					printf("Enemy detected\n\r");
					if (p_xAgent->IsType("Defender"))
					{
						printf("Defender\n\r");
					}
					return;
				}
				else {
					if (p_xAgent->IsType("Attacker"))
					{
						p_xAgent->set_My_State(AISTATE::MOVE);
					}
					else if (p_xAgent->IsType("Defender"))
					{
						p_xAgent->set_My_State(AISTATE::IDLE);
					}
				}
			}
		}
	}

}

void AIControlCentral::spawn_Walls(sf::Vector2i p_vPosition)
{
	if (m_iMaxWalls < m_iCurrentWalls)
	{
		return;
	}

	Walls *xWall = new Walls;
	int x = p_vPosition.x / 32;
	x = x * 32;
	int y = p_vPosition.y / 32;
	y = y * 32;

	for (unsigned int i = 0; i < m_avNodes.size(); i++)
	{
		if (m_avNodes[i]->getPos().x == x && m_avNodes[i]->getPos().y == y)
		{
			if (m_avNodes[i]->getOccupied() || m_avNodes[i]->getIsGoal())
			{
				delete xWall;
				xWall = nullptr;
				return;
			}
			m_avNodes[i]->setOccupied(true);
			xWall->initialize(m_avNodes[i], m_xWindow);
			break;
		}
	}
	m_xWalls.push_back(xWall);

	m_iCurrentWalls++;

}


void AIControlCentral::spawn_Defender(sf::Vector2i p_vPosition)
{
	if (m_iMaxDefenders <= m_iCurrentDefenders)
	{
		return;
	}

	Defender* xDefender = new Defender();
	int x = p_vPosition.x / 32;
	x = x * 32;
	int y = p_vPosition.y / 32;
	y = y * 32;

	for (unsigned int i = 0; i < m_avNodes.size(); i++)
	{
		if (m_avNodes[i]->getPos().x == x && m_avNodes[i]->getPos().y == y)
		{
			if (m_avNodes[i]->getOccupied() || m_avNodes[i]->getIsGoal())
			{
				delete xDefender;
				xDefender = nullptr;
				return;
			}
			m_avNodes[i]->setOccupied(true);
			xDefender->initialize(m_avNodes[i], m_xWindow);
			break;
		}
	}
	m_xAgents.push_back(xDefender);
	m_iCurrentDefenders++;

}

void AIControlCentral::spawn_Attackers()
{
	if (m_iCurrentAttackers <= m_iMaxAttackers)
	{

		printf("Spawning Attackers\n\r");

		int y = ::GetTickCount() % 15;
		int x = ::GetTickCount() % 4;

		Attacker* xAttacker = new Attacker();
		y *= 32;
		x *= 32;

		for (unsigned int i = 0; i < m_avNodes.size(); i++)
		{
			if (m_avNodes[i]->getPos().x == x && m_avNodes[i]->getPos().y == y)
			{
				if (m_avNodes[i]->getOccupied())
				{
					delete xAttacker;
					xAttacker = nullptr;
					return;
				}
				m_avNodes[i]->setOccupied(true);
				xAttacker->initialize(m_avNodes[i], m_xWindow);
				break;
			}
		}
		m_xAgents.push_back(xAttacker);
		m_iCurrentAttackers++;
	}

}

void AIControlCentral::start_AI()
{
	if (/*m_iCurrentDefenders == m_iMaxDefenders && */m_bStartGame == false)
	{
		m_bStartGame = true;
		printf("Game started\n\r");
	}

}

bool AIControlCentral::game_Running()
{
	return m_bStartGame;
}

int AIControlCentral::get_Defender_Count()
{

	return m_iCurrentDefenders;

}

int AIControlCentral::get_Attacker_Count()
{
	return m_iCurrentAttackers;
}

void AIControlCentral::remove_DeadAgents()
{
	std::vector<AI*> vDeadAttackers;
	std::vector<AI*> vDeadDefenders;
	for (int i = m_xAgents.size() - 1; i >= 0; i--)
	{
		if (m_xAgents[i]->get_My_State() == AISTATE::DEAD)
		{
			if (m_xAgents[i]->IsType("Defender"))
			{
				m_iCurrentDefenders--;
				vDeadDefenders.push_back(m_xAgents[i]);
			}
			else
			{
				m_iCurrentAttackers--;
				vDeadAttackers.push_back(m_xAgents[i]);
			}
			m_xAgents.erase(m_xAgents.begin() + i);
			//i--;
		}

	}

	for (unsigned int i = 0; i < m_xAgents.size(); i++)
	{
		for (unsigned int j = 0; j < vDeadAttackers.size(); j++)
		{
			if (m_xAgents[i]->get_CurrentAttackTarget() == nullptr)
			{
				continue;
			}
			else if (m_xAgents[i]->get_CurrentAttackTarget() == vDeadAttackers[j])
			{
				m_xAgents[i]->clear_Target();
			}
		}
		for (unsigned int j = 0; j < vDeadDefenders.size(); j++)
		{
			if (m_xAgents[i]->get_CurrentAttackTarget() == nullptr)
			{
				continue;
			}
			else if (m_xAgents[i]->get_CurrentAttackTarget() == vDeadDefenders[j])
			{
				m_xAgents[i]->clear_Target();
			}
		}
	}

	for (unsigned int i = 0; i < vDeadAttackers.size(); i++)
	{
		vDeadAttackers[i]->get_position()->setOccupied(false);
		delete vDeadAttackers[i];
		vDeadAttackers[i] = nullptr;
	}

	for (unsigned int i = 0; i < vDeadDefenders.size(); i++)
	{
		vDeadDefenders[i]->get_position()->setOccupied(false);
		delete vDeadDefenders[i];
		vDeadDefenders[i] = nullptr;
	}


	/*m_xAgents[i]->cleanup();
	delete m_xAgents[i];
	m_xAgents[i] = nullptr;
	*/


	//p_xAgent->cleanup();
	//p_xAgent->get_CurrentAttackTarget()->set_AttackTarget(nullptr);
	//delete p_xAgent;
	//p_xAgent = nullptr;

}
