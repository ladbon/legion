//pathfinding.h

#pragma once

#ifndef PATHFINDING_H_

#define PATHFINDING_H_

#include "node.h"
#include "Sprite.h"

class pathFinder
{
public:
	pathFinder();
	~pathFinder();

	std::vector<Node*> makePath(Node* pn_startNode, std::vector<Node*> pv_targetNodes);

	void Update(float delta);
	void Initialize(const sf::RenderWindow* p_xWindow);
	void Draw();

	Node* GetGoalNode(Node* p_xCurrentPosition, std::vector<Node*> p_xTargetNodes);

private:
	Node* mn_curNode;
	const sf::RenderWindow* m_xWindow;
	Sprite m_xOpen;
	Sprite m_xClosed;

	std::vector<Node*> m_xClosedNodes;
	std::vector<Node*> m_xOpenNodes;
	std::vector<Sprite> m_xSearchPattern;

};

#endif // !PATHFINDING_H_