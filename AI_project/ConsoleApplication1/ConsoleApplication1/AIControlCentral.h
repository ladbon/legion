// AIControlCentral.h

#pragma once

#include "Walls.h"

#ifndef AICONTROLCENTRAL_H_
#define AICONTROLCENTRAL_H_

/*
This is the puppet master.
It keeps a list of the attackers and defenders
it tells them what to do, where to go, which target to attack etc.

This is also the thing that spawns the agents.
Normally I'd do a register agent method, but I'm not so sure we need that.

*/

class AIControlCentral
{
public:
	AIControlCentral();
	~AIControlCentral();

	/*
	Idea is that this class takes care of all the Agents in the scene
	So all we should need is this class that tells agents where to go
	
	*/
	void initialize(const sf::RenderWindow* p_xWindow, std::vector<Node*> p_avNodes); // not ideal to send the window pointer here, but I need it
	void cleanup();
	void update(float p_fDelta);

	void agentsActions(AI* p_xAgent);

	// These are just methods so that the switch case on the agents actions gets a bit more tidy
	// that's why Defender/Attacker pointer p_xAgent is sent with. It is the agent that we are checking.
	

	// think I want to have the different AIs be aware of their surroundings so that if the agent attacks
	// it has a target that it can use and move towards.

	void attacker_Attack (Attacker* p_xAgent);
	void attacker_Move   (Attacker* p_xAgent);
	void attacker_Blocked(Attacker* p_xAgent);
	void attacker_Dead   (Attacker* p_xAgent);

	void defender_Attack (Defender* p_xAgent);
	void defender_Move   (Defender* p_xAgent);
	void defender_Idle   (Defender* p_xAgent);
	void defender_Dead   (Defender* p_xAgent);
	void defender_Blocked(Defender* p_xAgent);
	
	void detect_enemy(AI* p_xAgent, Node* p_xCurrLocation);

	void remove_DeadAgents();

	void spawn_Defender(sf::Vector2i p_vPosition);
	void spawn_Attackers();
	void spawn_Walls(sf::Vector2i p_vPosition);

	void start_AI();
	bool game_Running();
	void draw_InnerWorkings();
	bool check_Goal();

	int get_Defender_Count();
	int get_Attacker_Count();

	unsigned int m_iMaxDefenders; // yes made it public in case we want to modify it from the outside - case of waves;
	unsigned int m_iMaxAttackers;
	unsigned int m_iMaxWalls;

private:
	// don't need this as a pointer. If I'm right we only need it here.
	pathFinder m_xPathFinder; 
	std::vector<AI*> m_xAgents;
	std::vector<Node*> m_avNodes;
	std::vector<Walls*> m_xWalls;
	float m_fAgentActionInterval;
	float m_fAgentActionTimer;

	unsigned int m_iCurrentDefenders;
	unsigned int m_iCurrentAttackers;
	unsigned int m_iCurrentWalls;

	const sf::RenderWindow* m_xWindow;
	bool m_bStartGame;
	bool m_bInnerWorking;
	bool m_bInGoal;
};

#endif // AICONTROLCENTRAL_H_
