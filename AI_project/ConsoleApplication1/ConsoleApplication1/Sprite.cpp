#include "Sprite.h"
// Ladbon

Sprite::Sprite()
{
}

Sprite::~Sprite()
{
	
}

void Sprite::set_image(std::string p_sFilename, sf::IntRect p_iPosition_size)
{
	m_iImage.loadFromFile(p_sFilename);

	m_tTexture.loadFromImage(m_iImage/*, p_iPosition_size*/); // <- The commented out code makes the image unable to load for some reason. I'm probably using this wrong, but wasn't quite sure how it was intended to be used

	m_sSprite.setTexture(m_tTexture, true);
	// not a fan of warnings so casting them to float to get rid of a warning
	m_sSprite.setPosition((float)p_iPosition_size.left, (float)p_iPosition_size.top); // just temp for testing so remove it whenever
}
void Sprite::set_position(sf::Vector2f p_vPosition)
{
	m_sSprite.setPosition(p_vPosition);
}
void Sprite::draw_image(const sf::RenderWindow* p_rWindow)
{
	const_cast<sf::RenderWindow*>(p_rWindow)->draw(m_sSprite);

}
sf::Sprite& Sprite::get_sprite()
{
	return m_sSprite;
}

