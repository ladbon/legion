// AI.h 

#pragma once

#ifndef AI_H
#define AI_H
#include <string>
#include <vector>
#include "pathfinding.h"
#include "node.h"

enum AISTATE
{
	ATTACK = 0,
	MOVE,
	BLOCKED, 
	IDLE, // for the defenders
	DEAD,
	NUMACTIONS, // not sure we need this ever, just a habit
};

class AI // base for AI
{
public:
	inline AI() {};
	inline ~AI() {};

	virtual bool IsType(std::string p_sType) = 0;

	virtual AISTATE get_My_State() = 0;
	virtual void set_My_State(AISTATE p_eState) = 0;
	//virtual void pathfind_Defatt() = 0;

	virtual void update(float p_fDelta) = 0;
	virtual void cleanup() = 0;

	virtual Node* get_position() = 0;

	virtual void set_AttackTarget(AI* p_xTarget) = 0;
	virtual AI* get_CurrentAttackTarget() = 0;
	virtual void clear_Target() = 0;

	// add the other stuff you want to access here as pure virtual.

};

#endif // AI_H