//node.h
#pragma once
#include <SFML/Graphics.hpp>

class Node
{
public:

	Node(float pf_Xpos, float pf_Ypos, int pi_orderNum);
	~Node();

	void update();

	void draw();

	void addGH();

	void resetPath();

	sf::Vector2f getPos();
	int getNum();

	void setH(int pi_inH);
	int getH();

	void setG(int pi_inG);
	int getG();
	
	void setF(int pi_inF);
	int getF();


	void setParent(Node* pn_inNode);
	Node* getParent();

	void setNeiUp(Node* pn_inNode);
	void setNeiDwn(Node* pn_inNode);
	void setNeiLft(Node* pn_inNode);
	void setNeiRgt(Node* pn_inNode);

	Node* getNeiUp();
	Node* getNeiDwn();
	Node* getNeiLft();
	Node* getNeiRgt();

	void setTerrain(int pi_newTerr);
	int getTerrain();

	void setOccupied(bool pb_newOcc);
	bool getOccupied();

	void setIsGoal(bool pb_isGoal);
	bool getIsGoal();

private:
	float mf_Xpos;
	float mf_Ypos;

	int mi_orderNum;

	Node* mn_parent;

	Node* mn_NeiUp;
	Node* mn_NeiDwn;
	Node* mn_NeiLft;
	Node* mn_NeiRgt;

	int mi_H;
	int mi_G;
	int mi_F;

	int mi_terrain;

	bool mb_isOccupied;
	bool mb_isGoal;
};