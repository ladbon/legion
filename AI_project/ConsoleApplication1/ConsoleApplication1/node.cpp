//node.cpp

#include "node.h"


Node::Node(float pf_Xpos, float pf_Ypos, int pi_orderNum)
{
	mf_Xpos = pf_Xpos; // fixed a bug here mf_pos = mf_pos;
	mf_Ypos = pf_Ypos;
	mi_orderNum = pi_orderNum;

	mn_parent = nullptr;

	mn_NeiUp = nullptr;
	mn_NeiDwn = nullptr;
	mn_NeiLft = nullptr;
	mn_NeiRgt = nullptr;

	mi_H = 0;
	mi_G = 0;
	mi_F = 0;

	mi_terrain = 10;

	mb_isOccupied = false;
	mb_isGoal = false;

#ifdef _DEBUG
	printf("Node Position x: %f - y: %f\n\r", mf_Xpos, mf_Ypos);

#endif // DEBUG


};

Node::~Node()
{

}


void Node::update()
{
	if (mi_H != 0)
		mi_H = 0;

	if (mi_G != 0)
		mi_G = 0;

	if (mi_F != 0)
		mi_F = 0;
	
	if (mn_parent != nullptr)
		mn_parent = nullptr;

};

void Node::addGH()
{
	mi_F = mi_G + mi_H;
};

void Node::resetPath()
{
	if (mi_H != 0)
		mi_H = 0;

	if (mi_G != 0)
		mi_G = 0;

	if (mi_F != 0)
		mi_F = 0;

	if (mn_parent != nullptr)
		mn_parent = nullptr;

};

sf::Vector2f Node::getPos()
{
	return sf::Vector2f(mf_Xpos,mf_Ypos);
};

int Node::getNum()
{
	return mi_orderNum;
}

void Node::setH(int pi_inH)
{
	mi_H = pi_inH;
}

int Node::getH()
{
	return mi_H;
}

void Node::setG(int pi_inG)
{
	mi_G = pi_inG;
}

int Node::getG()
{
	return mi_G;
}

void Node::setF(int pi_inF)
{
	mi_F = pi_inF;
}

int Node::getF()
{
	return mi_F;
}



void Node::setParent(Node* pn_inNode)
{
	mn_parent = pn_inNode;
}

Node* Node::getParent()
{
	return mn_parent;
}

void Node::setNeiUp(Node* pn_inNode)
{
	mn_NeiUp = pn_inNode;
}

void Node::setNeiDwn(Node* pn_inNode)
{
	mn_NeiDwn = pn_inNode;
}

void Node::setNeiLft(Node* pn_inNode)
{
	mn_NeiLft = pn_inNode;
}

void Node::setNeiRgt(Node* pn_inNode)
{
	mn_NeiRgt = pn_inNode;
}

Node* Node::getNeiUp()
{
	return mn_NeiUp;
}

Node* Node::getNeiDwn()
{
	return mn_NeiDwn;
}

Node* Node::getNeiLft()
{
	return mn_NeiLft;
}

Node* Node::getNeiRgt()
{
	return mn_NeiRgt;
}

void Node::setTerrain(int pi_newTerr)
{
	mi_terrain = pi_newTerr;
}

int Node::getTerrain()
{
	return mi_terrain;
}

void Node::setOccupied(bool pb_newOcc)
{
	mb_isOccupied = pb_newOcc;
}

bool Node::getOccupied()
{
	return mb_isOccupied;
}

void Node::setIsGoal(bool pb_isGoal)
{
	mb_isGoal = pb_isGoal;
}
bool Node::getIsGoal()
{
	return mb_isGoal;
}