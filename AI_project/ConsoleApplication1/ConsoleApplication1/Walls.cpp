#include "Walls.h"


Walls::Walls()
{
}


Walls::~Walls()
{
}

void Walls::update(float p_fdeltatime)
{
	ms_Sprite.draw_image(m_xWindow);
}

void Walls::initialize(Node* p_nPlacement, const sf::RenderWindow* p_xWindow)
{
	ms_Sprite.set_image("../images/wall.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	m_xWindow = p_xWindow;
}
