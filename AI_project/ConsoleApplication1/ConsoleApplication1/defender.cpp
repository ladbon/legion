#include <string>
#include <random>
#include "Sprite.h"
#include "defender.h"
#include "attacker.h"


Defender::Defender()
{
	// ms_Sprite = new Sprite();
	//mx_Target = new Defender();
	mn_Destination = nullptr;

	// name generator
	static std::default_random_engine generator;
	std::uniform_int_distribution<int> distribution(0, 2);
	mi_name_generator = distribution(generator);

	ms_defender[0] = "../images/defender1.png";
	ms_defender[1] = "../images/defender2.png";
	ms_defender[2] = "../images/defender3.png";
	m_sType = "Defender";

	m_idmg = 20;
	m_ihp = 100;
	m_eMyState = AISTATE::IDLE;
}

Defender::~Defender()
{

}

void Defender::update(float p_fdeltatime)
{
	/*
	if()
	{
	win();
	}
	*/
	if (m_eMyState == AISTATE::ATTACK)
	{
		m_sAttack.draw_image(m_xWindow);
	}
	else
	{
		ms_Sprite.draw_image(m_xWindow);
	}
}

void Defender::initialize(Node* p_nPlacement, const sf::RenderWindow* p_xWindow)
{
	m_xCurrNode = p_nPlacement;
	ms_Sprite.set_image(ms_defender[mi_name_generator], sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));

	if (mi_name_generator == 0)
	{
		m_sAttack.set_image("../images/defender1_attack.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	}
	else if (mi_name_generator == 1)
	{
		m_sAttack.set_image("../images/defender2_attack.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	}
	else if (mi_name_generator == 2)
	{
		m_sAttack.set_image("../images/defender3_attack.png", sf::IntRect((int)p_nPlacement->getPos().x, (int)p_nPlacement->getPos().y, 32, 32));
	}

	m_xWindow = p_xWindow;
}

void Defender::cleanup()
{
	m_xCurrNode->setOccupied(false);
}

//void Defender::win()
//{
//	/*
//	sprite->set_image("../images/win.png, sf::IntRect(0, 0, 960, 480));
//	*/
//}

int Defender::get_hp()
{
	return m_ihp;
}
bool Defender::IsType(std::string p_sType)
{
	// this won't print in the release version
#ifdef _DEBUG 
	static int once = 0;
	if (once == 0)
		printf("Defender type - %s\n\r", m_sType.c_str());

	once++;
#endif // _DEBUG

	return p_sType.compare(m_sType) == 0;
}


void Defender::give_dmg(int p_idmg)
{
	m_ihp -= p_idmg;
	if (m_ihp <= 0) // might as well set this here
	{
		m_eMyState = AISTATE::DEAD;
	}

}

int Defender::get_dmg()
{
	return m_idmg;
}

AISTATE Defender::get_My_State()
{
	return m_eMyState;
}


void Defender::set_My_State(AISTATE p_eState)
{
	m_eMyState = p_eState;
}

//void Defender::pathfind_Defatt()
//{
//	
//}

void Defender::set_position(Node* p_xNode)
{
	if (!p_xNode->getOccupied())
	{
		m_xCurrNode->setOccupied(false);
		m_xCurrNode = p_xNode;
		m_xCurrNode->setOccupied(true);
	}
}

void Defender::set_AttackTarget(AI* p_xTarget)
{
	mx_Target = (Attacker*)p_xTarget;
}


AI* Defender::get_CurrentAttackTarget()
{
	return (AI*)mx_Target;
}

void Defender::clear_Target()
{
	if (mx_Target != nullptr)
	{
		mx_Target->clear_Target();
	}
	mx_Target = nullptr;
}