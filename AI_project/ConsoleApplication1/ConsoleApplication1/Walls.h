#pragma once

#include "Sprite.h"
#include "node.h"

#ifndef WALLS_H_
#define WALLS_H_

class Walls
{
public:
	Walls();
	~Walls();
	void initialize(Node* p_nPlacement, const sf::RenderWindow* p_xWindow);
	void update(float p_fdeltatime);


private:
	Sprite ms_Sprite;
	const sf::RenderWindow* m_xWindow;


};



#endif // !WALLS_H_