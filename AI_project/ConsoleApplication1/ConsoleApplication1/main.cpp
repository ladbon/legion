#include <SFML/Graphics.hpp>
#include <iostream>
#include "Sprite.h"
#include "defender.h"
#include "attacker.h"
#include "node.h"
#include "AIControlCentral.h"
#include <conio.h>
//#include "vld.h"

int main()
{
	// Window /Ladbon

	sf::RenderWindow* window = new sf::RenderWindow(sf::VideoMode(960, 480), "Legion", sf::Style::Default);
	window->setFramerateLimit(60);

	// Deltatime /Ladbon

	sf::Clock cClock;
	cClock.restart();

	//float timer = 1.0f;
	float fDeltatime;

	// Background Image /Ladbon

	Sprite background;
	background.set_image("../images/background.png", sf::IntRect(0, 0, 960, 480));


	//// A list of all the starting positions for the entities

	//std::vector<sf::Vector2i>positions;

	// Defender list /Ladbon // No longer needed / Inge
	// std::vector<Defender*> army; /Ladbon
	// Attackers list // not needed anymore
	//std::vector<Attacker*> invaders;


	// Nodes are created here

	std::vector<Node*> mv_nodeGrid;

	int i_counter = 0;

	for (int y = 0; y < 15; y++)
	{
		for (int x = 0; x < 30; x++)
		{
			Node* newNode = new Node(x * 32.0f, y * 32.0f, i_counter);
			if (y > 4 && y < 9 && x > 25)
			{
				// this is the veritas universalis - Uppsala logo
				newNode->setIsGoal(true);
			}

			if (y > 4 && y < 9 && x == 25)
			{
				// These are the wall nodes
				newNode->setOccupied(true);
			}
			


			if (x > 0 && x < 30 && y < 15) // set the left and right neighbor
			{
				newNode->setNeiLft(mv_nodeGrid[(x - 1) + (y * 30)]);
				mv_nodeGrid[(x - 1) + (y * 30)]->setNeiRgt(newNode);
			}
			if (y > 0 && y < 15) // set the up and down neighbor
			{
				newNode->setNeiUp(mv_nodeGrid[(y - 1) * 30 + x]);
				mv_nodeGrid[(y - 1) * 30 + x]->setNeiDwn(newNode);
			}

			mv_nodeGrid.push_back(newNode);
			i_counter++;
		}
	}

	AIControlCentral m_xAgentCenter;
	m_xAgentCenter.initialize(window, mv_nodeGrid);

	// Game loop and 
	bool once = false;
	sf::Event event;


	while (window->isOpen())
	{
		// Deltatime
		fDeltatime = cClock.getElapsedTime().asSeconds();
		cClock.restart();
		window->clear();

		if (m_xAgentCenter.game_Running())
		{
			if (m_xAgentCenter.get_Defender_Count() <= 0)
			{
				background.set_image("../images/lose.png", sf::IntRect(0, 0, 960, 480));
				once = true;
			}
			else if (m_xAgentCenter.get_Attacker_Count() <= 0)
			{
				background.set_image("../images/win.png", sf::IntRect(0, 0, 960, 480));
				once = true;
			}
		}
		 if (m_xAgentCenter.get_Attacker_Count() > 0 &&
			m_xAgentCenter.get_Defender_Count() > 0 &&
			m_xAgentCenter.check_Goal())
		{
			background.set_image("../images/lose.png", sf::IntRect(0, 0, 960, 480));
			once = true;
		}
		while (window->pollEvent(event))
		{
			switch (event.type)
			{
			case sf::Event::Closed:
				window->close();
				break;
			case sf::Event::MouseButtonPressed:
				if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && !once)
				{
					m_xAgentCenter.spawn_Defender(sf::Mouse::getPosition(*window));
				}
				if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !once)
				{
					m_xAgentCenter.spawn_Walls(sf::Mouse::getPosition(*window));
				}
				break;
			case sf::Event::KeyPressed:
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) && !once)
				{
					m_xAgentCenter.spawn_Attackers();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::D) && !once)
				{
					m_xAgentCenter.start_AI();
					once = true;
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
				{
					window->close();
				}
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
				{
					m_xAgentCenter.draw_InnerWorkings();
				}
				break;
			}

		}
		//}

		background.draw_image(window);

		m_xAgentCenter.update(fDeltatime); // <- updates all entities (position, and AI behavior)


		window->display();
	
	}

	m_xAgentCenter.cleanup();

	for (unsigned int i = 0; i < mv_nodeGrid.size(); i++)
	{
		delete mv_nodeGrid[i];
		mv_nodeGrid[i] = nullptr;
	}
	mv_nodeGrid.clear();

	delete window;
	window = nullptr;

	return 0;
}


// code graveyard - didn't want to remove it in case you want to use some of it

/* Update rightclick comment

H�r fastnar spelet och spelaren f�r v�lja ut soldaterna.
ingen feedback indikation h�r. Spelaren helt enkelt v�ljer tile.
Noden tar in en screen position och hittar den r�tta positionen.
Den skickar informationen till sprite klassen och ritar ut den p� r�tt nod.
Den �ndrar mouse position till nodens position igenom att t�nka att nodens *area*
�r mellan x = 96-128 och y = 96-128 och d� returnar den en vector2f(96f, 96f).


S� d�r ritar den den ut n�r du klickar och l�gger gubben p� r�tt position.
Den �ven pushbackar soldaten till en lista som senare uppdateras och ritas ut omg�ende.

Defender* soldier = new defender(army);

soldier->set_image("../images/soldier.jpg",
node->get_node_position(sf::Mouse::getPosition().x, sf::Mouse::getPosition().y),
32, 32));

army.push_back(soldier);
/Ladbon
*/


// Update all entities - not needed anymore.
//timer -= _fDeltatime;
//if (timer <= 0.0f)
//{
//timer = 1.0f;
/*
H�r kallar ni till b�de def och attack funktion.
Draw kallar ni ifr�n update i era klasser eftersom sprite klassen tar ni in d�r.

for(std::vector<Defender*>::iterator itr = army.begin(); itr != army.end(); itr++)
{

if(itr.get_hp() <= 0)
{
itr.cleanup();
itr.erase();
itr =

}
else
{
(*itr)->update(_fDeltatime);
}

}

for(std::vector<Attacker*>::iterator itr = invaders.begin(); itr != invaders.end(); itr++)
{

if(itr.get_hp() <= 0)
{
itr.cleanup();
}
else
{
(*itr)->update(_fDeltatime);
}

}
/Ladbon
*/
//}


// Delete dead A.I's
/*
for (std::vector<Defender*>::iterator itr = army.begin(); itr != army.end(); itr++)
{



}
for(std::vector<Attacker*>::iterator itr = invaders.begin(); itr != invaders.end(); itr++)
{


}
*/
